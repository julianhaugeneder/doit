![alt text](https://i.imgur.com/RZwG0H4.png "DoIt")
# DoIt  
DoIt ist ein einfaches Todo Dashboard mit Kategorien.  

## Demo
Unter doit.haugeneder.dev findest du eine Demo des Projekts.

## Starte das Projekt lokal 
```
npm i
vue-cli-service serve
```
## Tested e2e with Cypress
```
vue-cli-service test:e2e
```
## Screenshots  
![alt text](https://i.imgur.com/etbELwY.png "DoIt")

## Dependencies
```
"core-js": "^3.6.5",
"vue": "^2.6.11",
"vue-router": "^3.2.0",
"vuetify": "^2.2.11"
"@vue/cli-plugin-babel": "~4.3.0",
"@vue/cli-plugin-eslint": "~4.3.0",
"@vue/cli-plugin-router": "~4.3.0",
"@vue/cli-service": "~4.3.0",
"@vue/eslint-config-prettier": "^6.0.0",
"babel-eslint": "^10.1.0",
"deepmerge": "^4.2.2",
"eslint": "^6.7.2",
"eslint-plugin-prettier": "^3.1.1",
"eslint-plugin-vue": "^6.2.2",
"fibers": "^5.0.0",
"node-sass": "^4.12.0",
"prettier": "^1.19.1",
"sass": "^1.26.5",
"sass-loader": "^8.0.2",
"vue-cli-plugin-vuetify": "^2.0.5",
"vue-masonry-css": "^1.0.3",
"vue-template-compiler": "^2.6.11",
"vuetify-loader": "^1.3.0"
```
## License

[ISC License ](https://choosealicense.com/licenses/isc/)

**Copyright (c) 2019, Julian Haugeneder** 

Permission to use, copy, modify, and/or distribute this software for any  
purpose with or without fee is hereby granted, provided that the above  
copyright notice and this permission notice appear in all copies.  

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES  
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF  
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR  
ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES  
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN  
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF  
OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
